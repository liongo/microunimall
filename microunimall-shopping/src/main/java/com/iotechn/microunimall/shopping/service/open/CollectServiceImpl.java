package com.iotechn.microunimall.shopping.service.open;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.iotechn.microunimall.core.Const;
import com.iotechn.microunimall.core.exception.AppServiceException;
import com.iotechn.microunimall.core.exception.ExceptionDefinition;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.data.starter.compent.CacheComponent;
import com.iotechn.microunimall.shopping.api.domain.CollectDO;
import com.iotechn.microunimall.shopping.api.dto.CollectDTO;
import com.iotechn.microunimall.shopping.api.service.biz.CollectBizService;
import com.iotechn.microunimall.shopping.api.service.biz.GoodsBizService;
import com.iotechn.microunimall.shopping.api.service.open.CollectService;
import com.iotechn.microunimall.shopping.mapper.CollectMapper;
import com.iotechn.microunimall.shopping.service.biz.CollectBizServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/*
@author kbq
@date  2019/7/5 - 10:48
*/
@Service("collectService")
public class CollectServiceImpl implements CollectService {

    @Autowired
    private CollectMapper collectMapper;

    @Autowired
    private CacheComponent cacheComponent;

    @Autowired
    private GoodsBizService goodsBizService;

    @Autowired
    private CollectBizService collectBizService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addCollect(Long userId, Long spuId) throws ServiceException {
        //校验SPU是否存在
        goodsBizService.getSpuById(spuId);
        List<CollectDO> collectDOS = collectMapper.selectList(new EntityWrapper<CollectDO>()
                .eq("user_id", userId)
                .eq("spu_id", spuId));
        if (!CollectionUtils.isEmpty(collectDOS)) {
            throw new AppServiceException(ExceptionDefinition.COLLECT_ALREADY_EXISTED);
        }
        CollectDO collectDO = new CollectDO(userId, spuId);
        Date now = new Date();
        collectDO.setGmtCreate(now);
        collectDO.setGmtUpdate(collectDO.getGmtCreate());
        cacheComponent.putSetRaw(CollectBizServiceImpl.CA_USER_COLLECT + userId, spuId + "", Const.CACHE_ONE_DAY);
        return collectMapper.insert(collectDO) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteCollect(Long userId, Long spuId) throws ServiceException {
        Integer num = collectMapper.delete(new EntityWrapper<CollectDO>()
                .eq("user_id", userId)
                .eq("spu_id", spuId)
        );
        if (num > 0) {
            cacheComponent.removeSetRaw(CollectBizServiceImpl.CA_USER_COLLECT + userId, spuId + "");
            return true;
        }

        throw new AppServiceException(ExceptionDefinition.PARAM_CHECK_FAILED);
    }

    @Override
    public Page<CollectDTO> getCollectAll(Long userId, Integer pageNo, Integer pageSize) throws ServiceException {
        Integer count = collectMapper.selectCount(new EntityWrapper<CollectDO>().eq("user_id", userId));
        Integer offset = pageSize * (pageNo - 1);
        List<CollectDTO> collectAll = collectMapper.getCollectAll(userId, offset, pageSize);
        Page<CollectDTO> page = new Page<CollectDTO>(collectAll, pageNo, pageSize, count);
        return page;
    }

    @Override
    public CollectDTO getCollectById(Long userId, Long collectId, Long spuId) throws ServiceException {
        return collectMapper.getCollectById(userId, collectId, spuId);
    }

    @Override
    public Boolean getCollectBySpuId(Long spuId, Long userId) throws ServiceException {
        return collectBizService.getCollectBySpuId(spuId, userId);
    }
}
