package com.iotechn.microunimall.shopping.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.shopping.api.domain.CategoryDO;

/**
 * Created by rize on 2019/7/2.
 */
public interface CategoryMapper extends BaseMapper<CategoryDO> {

}
