package com.iotechn.microunimall.shopping.service.open;

import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.shopping.api.dto.RecommendDTO;
import com.iotechn.microunimall.shopping.api.service.biz.RecommendBizService;
import com.iotechn.microunimall.shopping.api.service.open.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: kbq
 * Date: 2019-07-08
 * Time: 下午3:40
 */
@Service("recommendService")
public class RecommendServiceImpl implements RecommendService {

    @Autowired
    private RecommendBizService recommendBizService;

    @Override
    public List<RecommendDTO> getRecommendByType(Integer recommendType, Integer pageNo, Integer pageSize) throws ServiceException {
        return recommendBizService.getRecommendByType(recommendType,pageNo,pageSize);
    }
}
