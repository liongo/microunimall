package com.iotechn.microunimall.order.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.order.api.domain.OrderSkuDO;

/**
 * Created by rize on 2019/7/6.
 */
public interface OrderSkuMapper extends BaseMapper<OrderSkuDO> {


}
