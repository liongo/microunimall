package com.iotechn.microunimall.order;

import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallDefaultMysql;
import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallDefaultRedis;
import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallSeataGlobalTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisReactiveAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

@ImportResource("classpath:dubbo.xml")
@MapperScan({"com.iotechn.microunimall.order.mapper*"})
@EnableUnimallDefaultMysql
@EnableUnimallDefaultRedis
@EnableUnimallSeataGlobalTransaction
@SpringBootApplication(exclude = {RedisAutoConfiguration.class, RedisReactiveAutoConfiguration.class})
public class MicrounimallOrderApplication {

    private static final Logger logger = LoggerFactory.getLogger(MicrounimallOrderApplication.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MicrounimallOrderApplication.class, args);
        logger.info("[订单服务初始化完成]");
    }

}
