package com.iotechn.microunimall.fee.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.core.model.KVModel;
import com.iotechn.microunimall.fee.api.domain.CouponDO;
import com.iotechn.microunimall.fee.api.dto.CouponAdminDTO;
import com.iotechn.microunimall.fee.api.dto.CouponDTO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by rize on 2019/7/4.
 */
public interface CouponMapper extends BaseMapper<CouponDO> {

    public Integer decCoupon(Long couponId);

    //这样写MyBatis无法直接映射泛型，只能用Long了
    public List<KVModel<Long,Long>> getUserCouponsCount(@Param("userId") Long userId, @Param("couponIds") List<Long> couponIds);

    public List<CouponDTO> getActiveCoupons();

    public List<CouponAdminDTO> getAdminCouponList(@Param("title") String title, @Param("type") Integer type, @Param("status") Integer status, @Param("now") Date now, @Param("offset") Integer offset, @Param("limit") Integer limit);
}
