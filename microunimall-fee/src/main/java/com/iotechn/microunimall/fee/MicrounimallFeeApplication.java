package com.iotechn.microunimall.fee;

import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallDefaultMysql;
import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallDefaultRedis;
import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallSeataGlobalTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisReactiveAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

@ImportResource("classpath:dubbo.xml")
@MapperScan({"com.iotechn.microunimall.fee.mapper*"})
@EnableUnimallDefaultMysql
@EnableUnimallDefaultRedis
@EnableUnimallSeataGlobalTransaction
@SpringBootApplication(exclude = {RedisAutoConfiguration.class, RedisReactiveAutoConfiguration.class})
public class MicrounimallFeeApplication {

    private static final Logger logger = LoggerFactory.getLogger(MicrounimallFeeApplication.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MicrounimallFeeApplication.class, args);
        logger.info("[费用系统初始化成功]");
    }

}
