package com.iotechn.microunimall.fee.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.fee.api.domain.FreightTemplateDO;
import org.apache.ibatis.annotations.Param;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: kbq
 * Date: 2019-07-07
 * Time: 下午3:26
 */
public interface FreightTemplateMapper extends BaseMapper<FreightTemplateDO> {

    public FreightTemplateDO selectFreightBySkuId(@Param("skuId") Long skuId);
}
