package com.iotechn.microunimall.fee.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.fee.api.domain.FreightTemplateCarriageDO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: kbq
 * Date: 2019-07-07
 * Time: 下午3:28
 */
public interface FreightTemplateCarriageMapper extends BaseMapper<FreightTemplateCarriageDO> {
}
