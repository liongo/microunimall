package com.iotechn.microunimall.fee.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.fee.api.domain.UserCouponDO;
import com.iotechn.microunimall.fee.api.dto.UserCouponDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by rize on 2019/7/4.
 */
public interface UserCouponMapper extends BaseMapper<UserCouponDO> {

    public List<UserCouponDTO> getUserCoupons(Long userId);

    public UserCouponDTO getUserCouponById(@Param("userCouponId") Long userCouponId, @Param("userId") Long userId);

}
