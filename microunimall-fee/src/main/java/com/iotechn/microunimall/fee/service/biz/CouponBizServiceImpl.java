package com.iotechn.microunimall.fee.service.biz;

import com.iotechn.microunimall.fee.api.domain.UserCouponDO;
import com.iotechn.microunimall.fee.api.dto.UserCouponDTO;
import com.iotechn.microunimall.fee.api.service.biz.CouponBizService;
import com.iotechn.microunimall.fee.mapper.CouponMapper;
import com.iotechn.microunimall.fee.mapper.UserCouponMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/13
 * Time: 21:33
 */
@Service("couponBizService")
public class CouponBizServiceImpl implements CouponBizService {

    @Autowired
    private UserCouponMapper userCouponMapper;

    @Override
    public UserCouponDTO getUserCouponById(Long userCouponId, Long userId) {
        return userCouponMapper.getUserCouponById(userCouponId, userId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer updateById(UserCouponDO userCouponDO) {
        return userCouponMapper.updateById(userCouponDO);
    }
}
