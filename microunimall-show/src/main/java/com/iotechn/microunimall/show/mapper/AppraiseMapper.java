package com.iotechn.microunimall.show.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.show.api.domain.AppraiseDO;
import com.iotechn.microunimall.show.api.dto.AppraiseDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
@author kbq
@date  2019/7/6 - 10:17
*/
public interface AppraiseMapper extends BaseMapper<AppraiseDO> {

}
