package com.iotechn.microunimall.admin.api.exception;

import com.iotechn.microunimall.core.exception.ServiceExceptionDefinition;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/9
 * Time: 15:43
 */
public class AdminExceptionDefinition {

    public static final ServiceExceptionDefinition ADMIN_UNKNOWN_EXCEPTION =
            new ServiceExceptionDefinition(50000, "管理员系统未知异常");

    public static final ServiceExceptionDefinition ADMIN_NOT_EXIST =
            new ServiceExceptionDefinition(50001, "管理员不存在");

    public static final ServiceExceptionDefinition ADMIN_PASSWORD_ERROR =
            new ServiceExceptionDefinition(50002, "密码错误");

    public static final ServiceExceptionDefinition ADMIN_NOT_BIND_WECHAT =
            new ServiceExceptionDefinition(50003, "管理员尚未绑定微信");

    public static final ServiceExceptionDefinition ADMIN_APPLY_NOT_BELONGS_TO_YOU =
            new ServiceExceptionDefinition(50004, "用户申请表并不属于您");

    public static final ServiceExceptionDefinition ADMIN_APPLY_NOT_SUPPORT_ONE_KEY =
            new ServiceExceptionDefinition(50005, "未定义类型不支持一键发布");

    public static final ServiceExceptionDefinition ADMIN_ROLE_IS_EMPTY =
            new ServiceExceptionDefinition(50006, "管理员角色为空！");

    public static final ServiceExceptionDefinition ADMIN_USER_NAME_REPEAT =
            new ServiceExceptionDefinition(50007, "管理员用户名重复");

    public static final ServiceExceptionDefinition ADMIN_VERIFYCODE_ERROR =
            new ServiceExceptionDefinition(50008, "登陆验证码错误");

    public static final ServiceExceptionDefinition ADMIN_USER_NOT_EXITS =
            new ServiceExceptionDefinition(50009, "管理员不存在，请输入正确账号密码");

    public static final ServiceExceptionDefinition ADMIN_GUEST_NOT_NEED_VERIFY_CODE =
            new ServiceExceptionDefinition(50010, "游客用户无须验证码，请直接输入666666");

    public static final ServiceExceptionDefinition ADMIN_API_PERMISSION_REGISTER_FAILED =
            new ServiceExceptionDefinition(50011, "API权限注册失败");

    public static final ServiceExceptionDefinition ADMIN_NOT_LOGIN =
            new ServiceExceptionDefinition(50012, "管理员尚未登录");

}
