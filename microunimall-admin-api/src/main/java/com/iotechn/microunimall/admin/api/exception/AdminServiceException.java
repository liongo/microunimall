package com.iotechn.microunimall.admin.api.exception;

import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.exception.ServiceExceptionDefinition;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: kbq
 * Date: 2019-07-07
 * Time: 下午4:21
 */
public class AdminServiceException extends ServiceException implements Serializable {

    public AdminServiceException() {}

    public AdminServiceException(ServiceExceptionDefinition definition) {
        super(definition);
    }

    public AdminServiceException(String message, int code) {
        super(message,code);
    }

}
