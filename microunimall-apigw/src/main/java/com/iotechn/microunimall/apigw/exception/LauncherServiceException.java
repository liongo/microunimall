package com.iotechn.microunimall.apigw.exception;


import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.exception.ServiceExceptionDefinition;

/**
 * Created by rize on 2019/6/30.
 */
public class LauncherServiceException extends ServiceException {

    public LauncherServiceException(ServiceExceptionDefinition exceptionDefinition) {
        super(exceptionDefinition);
    }

}
