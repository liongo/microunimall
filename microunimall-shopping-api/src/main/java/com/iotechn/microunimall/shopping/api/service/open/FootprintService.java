package com.iotechn.microunimall.shopping.api.service.open;


import com.iotechn.microunimall.core.annotation.HttpMethod;
import com.iotechn.microunimall.core.annotation.HttpOpenApi;
import com.iotechn.microunimall.core.annotation.HttpParam;
import com.iotechn.microunimall.core.annotation.HttpParamType;
import com.iotechn.microunimall.core.annotation.param.NotNull;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.shopping.api.dto.FootprintDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: kbq
 * Date: 2019-07-08
 * Time: 上午8:33
 */

@HttpOpenApi(group = "footprint", description = "足迹")
public interface FootprintService {

    @HttpMethod(description = "删除足迹")
    public boolean deleteFootprint(
            @org.jetbrains.annotations.NotNull @HttpParam(name = "userId", type = HttpParamType.USER_ID, description = "用户ID") Long userId,
            @NotNull @HttpParam(name = "footprintId", type = HttpParamType.COMMON, description = "足迹Id") Long footprintId) throws ServiceException;

    @HttpMethod(description = "分页查询所有足迹,通过时间顺序")
    public List<FootprintDTO> getAllFootprint(
            @NotNull @HttpParam(name = "userId", type = HttpParamType.USER_ID, description = "用户ID") Long userId) throws ServiceException;

}
