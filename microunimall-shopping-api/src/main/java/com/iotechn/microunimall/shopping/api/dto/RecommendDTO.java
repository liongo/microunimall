package com.iotechn.microunimall.shopping.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: kbq
 * Date: 2019-07-08
 * Time: 下午3:30
 */
@Data
public class RecommendDTO extends SuperDTO implements Serializable {

    private Integer recommendType;

    private Long spuId;

    private Integer spuOriginalPrice;

    private Integer spuPrice;

    private Integer spuVipPrice;

    private Integer spuSales;

    private String spuImg;

    private String spuTitle;

    private Long spuCategoryId;
}
