package com.iotechn.microunimall.shopping.api.dto;

import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.fee.api.dto.FreightTemplateDTO;
import com.iotechn.microunimall.shopping.api.domain.SkuDO;
import com.iotechn.microunimall.shopping.api.domain.SpuAttributeDO;
import com.iotechn.microunimall.show.api.dto.AppraiseDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rize on 2019/7/2.
 */
@Data
public class SpuDTO extends SuperDTO implements Serializable {

    private List<SkuDO> skuList;

    private Integer originalPrice;

    private Integer price;

    private Integer vipPrice;

    private Integer stock;

    private Integer sales;

    private String title;

    /**
     * 主图
     */
    private String img;

    /**
     * 后面的图，仅在详情接口才出现
     */
    private List<String> imgList;

    private String detail;

    private String description;

    private Long categoryId;

    private List<Long> categoryIds;

    private List<CategoryDTO> categoryList;

    private List<SpuAttributeDO> attributeList;

    /**
     * 商品的第一页(前10条)评价
     */
    private Page<AppraiseDTO> appraisePage;

    private String unit;

    private Long freightTemplateId;

    private FreightTemplateDTO freightTemplate;

    private Boolean collect;

    private Integer status;

}
