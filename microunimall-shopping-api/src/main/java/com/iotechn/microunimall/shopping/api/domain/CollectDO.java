package com.iotechn.microunimall.shopping.api.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
@author kbq
@date  2019/7/5 - 10:03
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("unimall_collect")
public class CollectDO extends SuperDO implements Serializable {
    @TableField("user_id")
    private Long userId;

    @TableField("spu_id")
    private Long spuId;

}
