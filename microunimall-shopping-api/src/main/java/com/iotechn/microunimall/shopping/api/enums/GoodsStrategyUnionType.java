package com.iotechn.microunimall.shopping.api.enums;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: kbq
 * Date: 2019-07-08
 * Time: 下午6:46
 */

public enum GoodsStrategyUnionType {

    SPU(1, "SPU"),
    SKU(2, "SKU"),
    CATEGORY(3, "类目"),
    ;

    GoodsStrategyUnionType(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private int code;

    private String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}

