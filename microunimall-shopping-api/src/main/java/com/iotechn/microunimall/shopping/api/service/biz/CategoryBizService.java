package com.iotechn.microunimall.shopping.api.service.biz;

import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.shopping.api.dto.CategoryDTO;
import com.iotechn.microunimall.shopping.api.dto.CategoryTreeNodeDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/10
 * Time: 16:13
 */
public interface CategoryBizService {

    public List<CategoryTreeNodeDTO> categorySecondLevelTree() throws ServiceException;

    public List<CategoryDTO> categoryList() throws ServiceException;

    public List<Long> getCategoryFamily(Long categoryId) throws ServiceException;

}
