package com.iotechn.microunimall.data.starter.annotaion;

import com.iotechn.microunimall.data.starter.config.tx.SeataConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/17
 * Time: 12:09
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({SeataConfiguration.class})
@Documented
public @interface EnableUnimallSeataGlobalTransaction {

}
