package com.iotechn.microunimall.data.starter.annotaion;

import com.iotechn.microunimall.data.starter.config.redis.RedisConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/8
 * Time: 10:50
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({RedisConfiguration.class})
@Documented
public @interface EnableUnimallDefaultRedis {
}
