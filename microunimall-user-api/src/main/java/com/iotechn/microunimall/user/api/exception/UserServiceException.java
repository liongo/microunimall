package com.iotechn.microunimall.user.api.exception;

import com.iotechn.microunimall.core.exception.ExceptionDefinition;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.exception.ServiceExceptionDefinition;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/9
 * Time: 13:49
 */
public class UserServiceException extends ServiceException implements Serializable {

    public UserServiceException() {}

    public UserServiceException(ServiceExceptionDefinition definition) {
        super(definition);
    }

}
