package com.iotechn.microunimall.user.api.util;

import com.alibaba.fastjson.JSONObject;
import com.iotechn.microunimall.user.api.dto.UserDTO;
import org.apache.dubbo.rpc.RpcContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/8
 * Time: 11:13
 */
public class SessionUtil {

    public static void setUser(UserDTO user) {
        RpcContext.getContext().setAttachment("user", JSONObject.toJSONString(user));
    }

    public static UserDTO getUser() {
        return JSONObject.parseObject(RpcContext.getContext().getAttachment("user"), UserDTO.class);
    }
}

