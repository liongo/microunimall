package com.iotechn.microunimall.user.api.service.biz;

import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.user.api.domain.UserDO;
import com.iotechn.microunimall.user.api.domain.UserFormIdDO;
import com.iotechn.microunimall.user.api.dto.UserDTO;
import com.iotechn.microunimall.user.api.model.WeChatCommonTemplateMessageModel;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/7
 * Time: 23:37
 */
public interface UserBizService {

    public String getWxH5AccessToken() throws Exception;

    public String getWxH5Ticket(String accessToken) throws Exception;

    public String getWxMiniAccessToken() throws Exception;

    public boolean sendWechatMiniTemplateMessage(WeChatCommonTemplateMessageModel model);

    public void setValidFormId(UserFormIdDO userFormIdDO);

    public UserFormIdDO getValidFormIdByUserId(Long userId);

    public UserDTO getUserDTOByAccessToken(String accessToken) throws ServiceException;

    public UserDO getUserById(Long userId) throws ServiceException;

}
