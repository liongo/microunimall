package com.iotechn.microunimall.user.api.exception;

import com.iotechn.microunimall.core.exception.ServiceExceptionDefinition;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/9
 * Time: 13:51
 */
public class UserExceptionDefinition {

    public static final ServiceExceptionDefinition USER_UNKNOWN_EXCEPTION =
            new ServiceExceptionDefinition(11000, "用户系统未知异常");

    public static final ServiceExceptionDefinition USER_SEND_VERIFY_FAILED =
            new ServiceExceptionDefinition(11001, "发送验证码失败");

    public static final ServiceExceptionDefinition USER_VERIFY_CODE_NOT_EXIST =
            new ServiceExceptionDefinition(11002, "验证码未发送或已过期");

    public static final ServiceExceptionDefinition USER_VERIFY_CODE_NOT_CORRECT =
            new ServiceExceptionDefinition(11003, "验证码不正确");

    public static final ServiceExceptionDefinition USER_PHONE_HAS_EXISTED =
            new ServiceExceptionDefinition(11004, "手机已经被注册");

    public static final ServiceExceptionDefinition USER_PHONE_NOT_EXIST =
            new ServiceExceptionDefinition(11005, "手机尚未绑定账号");

    public static final ServiceExceptionDefinition USER_PHONE_OR_PASSWORD_NOT_CORRECT =
            new ServiceExceptionDefinition(11006, "手机号或密码错误!");

    public static final ServiceExceptionDefinition USER_THIRD_PART_LOGIN_FAILED =
            new ServiceExceptionDefinition(11007, "用户第三方登录失败");

    public static final ServiceExceptionDefinition USER_THIRD_UNEXPECT_RESPONSE =
            new ServiceExceptionDefinition(11008, "第三方登录期望之外的错误");

    public static final ServiceExceptionDefinition USER_THIRD_PART_NOT_SUPPORT =
            new ServiceExceptionDefinition(11009, "未知的第三方登录平台");

    public static final ServiceExceptionDefinition USER_INFORMATION_MISSING =
            new ServiceExceptionDefinition(11010, "用户信息缺失，不能添加");

    public static final ServiceExceptionDefinition USER_PHONE_ALREADY_EXIST =
            new ServiceExceptionDefinition(11011, "用户电话已经存在，不能添加");

    public static final ServiceExceptionDefinition USER_CAN_NOT_ACTICE =
            new ServiceExceptionDefinition(11012, "用户处于冻结状态，请联系管理员");

    public static final ServiceExceptionDefinition USER_NOT_LOGIN =
            new ServiceExceptionDefinition(10001, "用户未登录");

}
