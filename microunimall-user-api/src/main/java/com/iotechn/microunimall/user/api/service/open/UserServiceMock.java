package com.iotechn.microunimall.user.api.service.open;

import com.iotechn.microunimall.core.exception.AppServiceException;
import com.iotechn.microunimall.core.exception.ExceptionDefinition;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.user.api.dto.UserDTO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/14
 * Time: 14:15
 */
public class UserServiceMock implements UserService {

    @Override
    public String sendVerifyCode(String phone) throws ServiceException {
        throw new AppServiceException(ExceptionDefinition.ADMIN_UNKNOWN_EXCEPTION);
    }

    @Override
    public String register(String phone, String password, String verifyCode, String ip) throws ServiceException {
        return null;
    }

    @Override
    public String bindPhone(String phone, String password, String verifyCode, Long userId) throws ServiceException {
        return null;
    }

    @Override
    public String resetPassword(String phone, String password, String verifyCode) throws ServiceException {
        return null;
    }

    @Override
    public UserDTO login(String phone, String password, Integer loginType, String raw, String ip) throws ServiceException {
        return null;
    }

    @Override
    public String logout(String accessToken, Long userId) throws ServiceException {
        return null;
    }

    @Override
    public UserDTO thirdPartLogin(Integer loginType, String ip, String raw) throws ServiceException {
        return null;
    }

    @Override
    public String syncUserInfo(String nickName, String nickname, String avatarUrl, Integer gender, Long birthday, String accessToken, Long userId) throws ServiceException {
        return null;
    }

    @Override
    public Object getH5Sign(String url) throws ServiceException {
        return null;
    }
}
