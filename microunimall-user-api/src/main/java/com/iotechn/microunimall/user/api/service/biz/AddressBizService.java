package com.iotechn.microunimall.user.api.service.biz;

import com.iotechn.microunimall.user.api.domain.AddressDO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/11
 * Time: 19:06
 */
public interface AddressBizService {

    public AddressDO getAddressById(Long id);

    public String create();

}
