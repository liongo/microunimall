package com.iotechn.microunimall.fee.api.service.biz;

import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.fee.api.dto.FreightCalcRequest;
import com.iotechn.microunimall.fee.api.dto.FreightTemplateDTO;
import com.iotechn.microunimall.fee.api.dto.ShipTraceDTO;

/**
 * Created with IntelliJ IDEA.
 * Description: 订单预览
 * User: rize
 * Date: 2019/11/10
 * Time: 16:12
 */
public interface FreightBizService {

    public Integer getFreightMoney(FreightCalcRequest freightCalcRequest) throws ServiceException;

    public FreightTemplateDTO getTemplateById(Long templateId) throws ServiceException;

    public ShipTraceDTO getShipTraceList(String shipNo, String shipCode) throws ServiceException;

}
