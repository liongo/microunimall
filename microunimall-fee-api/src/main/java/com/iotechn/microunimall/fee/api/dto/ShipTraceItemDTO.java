package com.iotechn.microunimall.fee.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by rize on 2019/7/10.
 */
@Data
public class ShipTraceItemDTO implements Serializable {

    private String station;

    private String time;

}
