package com.iotechn.microunimall.fee.api.dto;

import lombok.Data;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/18
 * Time: 10:25
 */
@Data
public class FreightCalcRequest {

    private String province;

    /**
     * 折后价格
     */
    private Integer price;

    /**
     * 策略 数量映射
     */
    private Map<Long, Integer> map;

}
