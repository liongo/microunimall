package com.iotechn.microunimall.show.api.service.open;

import com.iotechn.microunimall.core.annotation.HttpMethod;
import com.iotechn.microunimall.core.annotation.HttpOpenApi;
import com.iotechn.microunimall.core.annotation.HttpParam;
import com.iotechn.microunimall.core.annotation.HttpParamType;
import com.iotechn.microunimall.core.annotation.param.NotNull;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.show.api.dto.AppraiseDTO;

/*
@author kbq
@date  2019/7/6 - 10:18
*/
@HttpOpenApi(group = "appraise", description = "评价商品")
public interface AppraiseService {
//TODO    移动到订单服务中，用户实际上是对订单进行评价
//    @HttpMethod(description = "增加评价")
//    public Boolean addAppraise(
//            @NotNull @HttpParam(name = "appraiseRequestDTO", type = HttpParamType.COMMON, description = "来自订单的评价数据") AppraiseRequestDTO appraiseRequestDTO,
//            @NotNull @HttpParam(name = "userId", type = HttpParamType.USER_ID, description = "上传用户ID") Long userId) throws ServiceException;

    @HttpMethod(description = "根据评论Id删除评论")
    public Boolean deleteAppraiseById(
            @NotNull @HttpParam(name = "appraiseId", type = HttpParamType.COMMON, description = "评论ID") Long appraiseId,
            @NotNull @HttpParam(name = "userId", type = HttpParamType.USER_ID, description = "上传用户ID") Long userId) throws ServiceException;


    @HttpMethod(description = "查询用户所有评论")
    public Page<AppraiseDTO> getUserAllAppraise(
            @NotNull @HttpParam(name = "userId", type = HttpParamType.USER_ID, description = "上传用户ID") Long userId,
            @HttpParam(name = "pageNo", type = HttpParamType.COMMON, valueDef = "1", description = "查询页数") Integer pageNo,
            @HttpParam(name = "pageSize", type = HttpParamType.COMMON, valueDef = "10", description = "查询长度") Integer pageSize) throws ServiceException;


    @HttpMethod(description = "查询商品的所有评论")
    public Page<AppraiseDTO> getSpuAllAppraise(
            @NotNull @HttpParam(name = "spuId", type = HttpParamType.COMMON, description = "商品Id") Long spuId,
            @HttpParam(name = "pageNo", type = HttpParamType.COMMON, valueDef = "1", description = "查询页数") Integer pageNo,
            @HttpParam(name = "pageSize", type = HttpParamType.COMMON, valueDef = "10", description = "查询长度") Integer pageSize) throws ServiceException;

    @HttpMethod(description = "查询某一条评论")
    public AppraiseDTO getOneById(
            @NotNull @HttpParam(name = "userId", type = HttpParamType.USER_ID, description = "上传用户ID") Long userId,
            @NotNull @HttpParam(name = "AppraiseId", type = HttpParamType.COMMON, description = "评论Id") Long appraiseId) throws ServiceException;


}
