package com.iotechn.microunimall.show.api.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by rize on 2019/2/13.
 */
@Data
@TableName("unimall_img")
public class ImgDO extends SuperDO implements Serializable {

    @TableField("biz_type")
    private Integer bizType;

    @TableField("biz_id")
    private Long bizId;

    private String url;


}
