package com.iotechn.microunimall.order.api.dto;

import com.iotechn.microunimall.fee.api.dto.UserCouponDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rize on 2019/7/6.
 */
@Data
public class OrderRequestDTO implements Serializable {

    private List<OrderRequestSkuDTO> skuList;

    /**
     * 商品支付总价
     */
    private Integer totalPrice;

    private Integer totalOriginalPrice;

    private UserCouponDTO coupon;

    private Long addressId;

    private String mono;

    /**
     * 购物车 ？ 直接点击购买商品
     */
    private String takeWay;

    private Integer freightPrice;

}
